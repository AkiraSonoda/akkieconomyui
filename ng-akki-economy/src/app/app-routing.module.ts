import { NgModule } from '@angular/core';
import { Routes, RouterModule, ɵROUTER_PROVIDERS } from '@angular/router';
import { DialogComponent } from './components/dialog/dialog.component';
import { HomeComponent } from './components/home/home.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { StatusComponent } from './components/status/status.component';

const routes: Routes = [
  { path: 'dialog', component: DialogComponent },
  { path: 'home', component: HomeComponent },
  { path: 'inventory', component: InventoryComponent },
  { path: 'status', component: StatusComponent },
  { path: '404', component: HomeComponent},
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
